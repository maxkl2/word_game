use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::{fmt, io};
use std::io::{BufRead, BufReader, stdin, stdout, Write};
use std::path::Path;
use std::time::Instant;
use num_format::{Locale, ToFormattedString};
use rand::Rng;
use rand::seq::SliceRandom;

const N_WORDS: usize = 3;
const MIN_LEN: usize = 3;

type WordIndex = usize;

struct WordInfo {
    word: String,
    index: WordIndex
}

struct Riddle {
    common_word: String,
    trimmed: Vec<String>,
}

impl Display for Riddle {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?} ({})", self.trimmed, self.common_word)
    }
}

struct WordsIndex {
    words: Vec<WordInfo>,
    words_map: HashMap<String, WordIndex>,
    // contains: Vec<(Vec<WordIndex>, Vec<WordIndex>)>,
    contained: Vec<Vec<WordIndex>>
}

impl Display for WordsIndex {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        for info in &self.words {
            let word = &info.word;
            writeln!(f, "{}:", word)?;
            // let contains = &self.contains[info.index];
            // write!(f, "  Prefixes: ")?;
            // for prefix_index in &contains.0 {
            //     write!(f, "{}, ", self.words[*prefix_index].word)?;
            // }
            // writeln!(f)?;
            // write!(f, "  Suffixes: ")?;
            // for suffix_index in &contains.1 {
            //     write!(f, "{}, ", self.words[*suffix_index].word)?;
            // }
            // writeln!(f)?;
            write!(f, "  Contained in: ")?;
            for contained_index in &self.contained[info.index] {
                write!(f, "{}, ", self.get_word(*contained_index))?;
            }
            writeln!(f)?;
        }

        Ok(())
    }
}

impl WordsIndex {
    pub fn get_word(&self, index: WordIndex) -> &str {
        self.words[index].word.as_str()
    }

    pub fn from_words(words_raw: Vec<String>) -> WordsIndex {
        let start_index = Instant::now();

        let words: Vec<_> = words_raw.into_iter()
            .enumerate()
            .map(|(index, word)| WordInfo { word, index })
            .collect();

        let mut words_map = HashMap::new();
        for (index, info) in words.iter().enumerate() {
            words_map.insert(info.word.to_string(), index);
        }

        let t_index = start_index.elapsed();
        println!("Indexed in {:.2?}", t_index);

        let mut words_index = WordsIndex {
            words,
            words_map,
            // contains: Vec::new(),
            contained: Vec::new()
        };

        let start_search = Instant::now();

        words_index.search();

        let t_search = start_search.elapsed();
        println!("Searched pre-/suffixes in {:.2?}", t_search);

        words_index
    }

    fn search(&mut self) {
        // self.contains.reserve(self.words.len());
        self.contained.resize_with(self.words.len(), Vec::new);

        for info in &self.words {
            let word = &info.word;

            // let mut prefixes = Vec::new();
            // let mut suffixes = Vec::new();

            for (i, _) in word.char_indices().skip(MIN_LEN) {
                let (prefix, suffix) = word.split_at(i);
                if suffix.chars().count() < MIN_LEN {
                    continue;
                }

                // if let Some(prefix_index) = self.words_map.get(prefix) {
                //     let prefix_info = &self.words[*prefix_index];
                //     prefixes.push(prefix_info.index);
                //     self.contained[*prefix_index].push(info.index);
                // }
                //
                // if let Some(suffix_index) = self.words_map.get(suffix) {
                //     let suffix_info = &self.words[*suffix_index];
                //     suffixes.push(suffix_info.index);
                //     self.contained[*suffix_index].push(info.index);
                // }

                let prefix_index = self.words_map.get(prefix);
                let suffix_index = self.words_map.get(suffix);

                if let (Some(prefix_index), Some(suffix_index)) = (prefix_index, suffix_index) {
                    // Only if both parts were found in the word list (i.e. are actual words)

                    // let prefix_info = &self.words[*prefix_index];
                    // prefixes.push(prefix_info.index);
                    self.contained[*prefix_index].push(info.index);

                    // let suffix_info = &self.words[*suffix_index];
                    // suffixes.push(suffix_info.index);
                    self.contained[*suffix_index].push(info.index);
                }
            }

            // self.contains.push((prefixes, suffixes));
        }
    }

    fn random_combination_indices(&self) -> (WordIndex, Vec<WordIndex>) {
        let mut rng = rand::thread_rng();
        loop {
            let common_index = rng.gen_range(0..self.words.len());
            let contained = &self.contained[common_index];
            if contained.len() >= N_WORDS {
                let word_indices: Vec<_> = contained.choose_multiple(&mut rng, N_WORDS)
                    .cloned()
                    .collect();
                return (common_index, word_indices);
            }
        }
    }

    pub fn random_riddle(&self) -> Riddle {
        let (common_index, indices) = self.random_combination_indices();
        let common_word = self.get_word(common_index);
        Riddle {
            common_word: common_word.to_string(),
            trimmed: indices.iter()
                .map(|index| self.get_word(*index))
                .map(|word| {
                    match word.strip_prefix(common_word) {
                        None => word.strip_suffix(common_word),
                        Some(s) => Some(s)
                    }.expect("Common word is neither prefix nor suffix")
                })
                .map(|word| word.to_string())
                .collect()
        }
    }

    pub fn check_riddle(&self, riddle: &Riddle, solution: &str) -> bool {
        let solution = solution.to_lowercase();

        if !self.words_map.contains_key(&solution) {
            return false;
        }

        for trimmed in &riddle.trimmed {
            let prefixed = format!("{}{}", solution, trimmed);
            if self.words_map.contains_key(&prefixed) {
                continue;
            }

            let suffixed = format!("{}{}", trimmed, solution);
            if self.words_map.contains_key(&suffixed) {
                continue;
            }

            return false;
        }

        true
    }
}

/// Calculate the number of k-element combinations of n objects, without repetition
///
/// Source: https://stackoverflow.com/a/65563202
fn count_combinations(n: u64, k: u64) -> u64 {
    if k > n {
        0
    } else {
        (1..=k.min(n - k)).fold(1, |acc, val| acc * (n - val + 1) / val)
    }
}

fn main() {
    let start_load = Instant::now();

    let whitelists = vec![
        // "words/single.txt",
        // "words/simple.txt",
        // "words/strassen.txt",
        // "words/enz.txt",
        // "words/wordlist-german.txt",
        "words/nouns.txt",
        // "words/nouns_singular.txt",
    ];

    let blacklists: Vec<&str> = vec![
        // "words/blacklist.txt",
        // "words/enz-blacklist.txt",
    ];

    let mut whitelists_iter = whitelists.iter()
        .map(|path| load_wordlist(path).unwrap())
        .map(|words| words.into_iter().collect::<HashSet<_>>());
    let mut words_set = whitelists_iter.next().unwrap();
    for whitelist in whitelists_iter {
        words_set.extend(whitelist);
    }

    blacklists.iter()
        .map(|path| load_wordlist(path).unwrap())
        .map(|words| words.into_iter().collect::<HashSet<_>>())
        .for_each(|blacklist| {
            words_set = &words_set - &blacklist;
        });

    let words: Vec<_> = words_set.into_iter().collect();

    let t_load = start_load.elapsed();
    println!("Read {} words in {:.2?}", words.len(), t_load);

    let words_index = WordsIndex::from_words(words);

    // println!("{}", words_index);
    let mut total = 0;
    let mut n_basewords = 0;
    let mut stats = HashMap::new();
    for (index, contained) in words_index.contained.iter().enumerate() {
        let word = words_index.get_word(index);
        let combinations = count_combinations(contained.len() as u64, N_WORDS as u64);
        if combinations > 0 {
            total += combinations;
            n_basewords += 1;
            // println!("{}: {}", word, combinations);
        }
        let k = contained.len();
        let entry = stats.entry(k)
            .or_insert((0, Vec::new()));
        entry.0 += 1;
        if entry.1.len() < 3 {
            entry.1.push(word.to_string());
        }
    }
    println!("Total combinations: {} ({} different base words)", total.to_formatted_string(&Locale::de), n_basewords.to_formatted_string(&Locale::de));
    let mut stats_vec: Vec<_> = stats.into_iter().collect();
    stats_vec.sort_unstable_by_key(|(occurrences, _)| *occurrences);
    println!("Statistics:");
    for (occurrences, (n_words, ref words)) in stats_vec {
        println!(
            "  {} word{} part of {} other{} ({}{})",
            n_words,
            if n_words == 1 { "" } else { "s" },
            occurrences,
            if occurrences == 1 { "" } else { "s" },
            words.join(", "),
            if n_words > 3 { ", …" } else { "" }
        );
    }

    println!();
    println!("Commands: quit, reveal");
    println!();

    let mut riddle = words_index.random_riddle();
    println!("Riddle: {}", riddle.trimmed.join(", "));

    let mut stdout = stdout();
    print!("> ");
    stdout.flush()
        .unwrap();
    for line in stdin().lock().lines() {
        let line = line.unwrap();
        let line_trimmed = line.trim().to_lowercase();

        let mut new_riddle = false;

        match line_trimmed.as_str() {
            "quit" => break,
            "reveal" => {
                println!("The word was \"{}\"", riddle.common_word);
                new_riddle = true;
            },
            guess => {
                if words_index.check_riddle(&riddle, guess) {
                    if guess == riddle.common_word {
                        println!("Correct!");
                    } else {
                        println!("The intended word would have been \"{}\", but yours works just as well!", riddle.common_word);
                    }
                    new_riddle = true;
                } else {
                    println!("Wrong, try again!");
                }
            }
        }

        if new_riddle {
            riddle = words_index.random_riddle();
            println!();
            println!("New riddle: {}", riddle.trimmed.join(", "));
        }

        print!("> ");
        stdout.flush()
            .unwrap();
    }
}

fn load_wordlist<P: AsRef<Path>>(path: P) -> Result<Vec<String>, io::Error> {
    let mut words = Vec::new();

    let f = File::open(path)?;
    let mut reader = BufReader::new(f);
    let mut line = String::new();
    loop {
        line.clear();
        let size = reader.read_line(&mut line)?;
        if size == 0 {
            // Reached EOF
            break;
        }

        let word = line.trim();
        if word.len() > 0 {
            words.push(word.to_lowercase());
        }
    }

    Ok(words)
}
