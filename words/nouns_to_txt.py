import csv
import time

source = 'nouns.csv'
output = 'nouns.txt'

singulars = ['nominativ singular','nominativ singular*','nominativ singular 1','nominativ singular 2','nominativ singular 3','nominativ singular 4','nominativ singular stark','nominativ singular schwach','nominativ singular gemischt']
plurals = ['nominativ plural','nominativ plural*','nominativ plural 1','nominativ plural 2','nominativ plural 3','nominativ plural 4','nominativ plural stark','nominativ plural schwach','nominativ plural gemischt']

t_start = time.perf_counter()

n_total = 0
n_subst = 0
n_written = 0
with open(source, newline='') as csvfile:
    with open(output, 'w') as outfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            n_total += 1
            pos = row['pos']
            if pos == 'Substantiv':
                n_subst += 1

                singular = None
                for key in singulars:
                    word = row[key].strip()
                    if len(word) > 0:
                        singular = word
                        outfile.write(word + '\n')
                        n_written += 1
                        break

                for key in plurals:
                    word = row[key].strip()
                    if len(word) > 0:
                        if word != singular:
                            outfile.write(word + '\n')
                            n_written += 1
                        else:
                            #print(word)
                            pass
                        break


t_end = time.perf_counter()

print('Done in {:.3f} s'.format(t_end - t_start))
print(f'Read {n_total} words, {n_subst} of which nouns')
print(f'Wrote {n_written} words')
